/**
 * @file ftok.c
 * @author Mitsutoshi Nakano <ItSANgo@gmail.com>
 * @brief File to System V Ipc Key.
 * @version 0.1.0
 * @date 2020-01-02
 *
 * @copyright Copyright (c) 2020 Mitsutoshi Nakano
 * 
 */

#include <sys/types.h>
#include <sys/ipc.h>
#include <sysexits.h>
#include <stdlib.h>
#include <stdio.h>

/**
 * @brief help.
 * 
 * @param status exit status.
 * @param fp output file.
 * @param command command name.
 */
void
help(int status, FILE *fp, char command[])
{
    fprintf(fp, "usnage: %s proj_id [file path]\n", command);
    exit(status); /*NOTREACHED*/
}

/**
 * @brief main.
 * 
 * @param argc 
 * @param argv 
 * @return int 
 */
int
main(int argc, char *argv[])
{
    if (argc < 2) {
        help(EX_USAGE, stderr, argv[0]); /*NOTREACHED*/
    }
    int project_id = argv[1][0];
    char *path = (argc > 2) ? argv[2] : argv[0];
    key_t key = ftok(path, project_id);
    printf("%lx\n", (long) key);
    exit(EX_OK); /*NOTREACHED*/
}
